angular.module('app', []);
var app = angular.module('app');

app.controller('mainCtrl', function ($scope, $http) {

});

app.directive('tableSelect', function ($http) {
	return {
		controller: controller,
		link: link
	};

	function controller($scope) {
		this.setColumns = function (cols) {
			$scope.cols = cols;
		};
	}

	function link(scope, elem, attrs) {
		$http.get(attrs.resource).success(function (res) {
			scope.rows = res;
		});
	}
});

app.directive('columns', function () {
	return {
		require: ['^tableSelect', 'columns'],
		controller: controller,
		//templateUrl: "table-select.html",
		//replace: true,
		compile: compile,
		//transclude: true
	};

	function controller($scope) {
		var columns = [];
		this.addColumn = function (col) {
			columns.push(col);
		};
		this.getColumns = function () {
			return columns;
		};
	}

	function compile(tElem, tAttrs) {
		console.log('columns compile')
		var td = tElem.find('td')[0];
		return link;
	}

	function link(scope, elem, attrs, controllers, transFn) {
		console.log('columns link')
		var tableSelectController = controllers[0];
		var columnsController = controllers[1];
		tableSelectController.setColumns(columnsController.getColumns());
	}
});

app.directive('column', function () {
	return {
		require: '^columns',
		compile: compile,
	};

	function compile(tElem, tAttrs) {
		console.log('column compile')
		return link;
	}

	function link(scope, elem, attrs, columnsController, transFn) {
		console.log('column link')
		columnsController.addColumn({
			header: attrs.header,
			innerText: elem.html().trim(),
			repeatHere: attrs.repeat
		});
		console.log(attrs.repeat)
		elem.html('');
	}
});

app.directive('drawTable', function ($compile, $parse) {
	return {
		templateUrl: "table-select.html",
		replace: true,
		compile: compile
	};

	function compile(tElem, tAttrs) {
		return link;
	}

	function link(scope, elem, attrs) {
		scope.$watch('rows', function(o,n){
			if (o !== n) {
				var trOpen = '<tr ng-repeat="row in rows">';
				var trClose = '</tr>';
				var tdCollection = [];
				var cells = scope.cols
					.map(function (column, index) {
						return column.innerText
					});

				cells.forEach(function (text, index) {
					var tdElem;
					
					if (index === 0) {
						tdElem = '<td>' +
								 '<span>{{row.bundle}}</span>' +
							 	 '<span>' + 
							     '<input type="checkbox" ng-model="master" id="master">' + 
							 	 '<label for="master">All</label>' +
							 	 '</span>' + 
								 '<span ng-repeat="stuff in row.facilities">' +
								 text + 
								 '</span>' +
								 '</td>';
						
						tdCollection.push(tdElem);
						return;
					}
					
					tdElem = '<td class="p-t-60">' + 
							 '<span ng-repeat="stuff in row.facilities">' +
							 text + 
							 '</span>' +
						     '</td>';
					tdCollection.push(tdElem);
				});
				
				var html = trOpen + _.flatten(tdCollection) +trClose;
				console.log(html)
				var compiledText = $compile(html)(scope);
				console.log(compiledText)
			   	angular.element(document.getElementById('tbody')).append(compiledText);
			}
		});
		

		
		
//								 '<span>{{row.name}}</span>' +
//							 	 '<span>' + 
//							     '<input type="checkbox" ng-model="master" id="master">' + 
//							 	 '<label for="master">All</label>' +
//							 	 '</span>'+ 
		
			//		var html = '<tr ng-repeat="row in rows">' +
			//				   '<td ng-repeat="col in cols">' +
			//					cell						  +
			//				   '</td>'						  +
			//				   '</tr>';
			//		var compiledText = $compile(html)(scope);
			//		angular.element(document.getElementById('tbody')).append(compiledText);
	}
});